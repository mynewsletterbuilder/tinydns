.PHONY: build run data

IMAGE_BASE = jbanetwork/
IMAGE = tinydns
TAG = latest
MY_PWD = $(shell pwd)
D_PATH=/opt/tinydns

all: build

build:
	docker build -t $(IMAGE_BASE)$(IMAGE):$(TAG) -f $(MY_PWD)/Dockerfile $(MY_PWD)
ifneq (,$(findstring p,$(MAKEFLAGS)))
	docker push $(IMAGE_BASE)$(IMAGE)
endif

run:
	docker run -it --rm --read-only -p 53:53/udp -v $(MY_PWD):$(D_PATH) $(IMAGE_BASE)$(IMAGE):$(TAG)

data:
	docker run -it --rm -v $(MY_PWD):$(D_PATH) $(IMAGE_BASE)$(IMAGE):$(TAG) tinydns-data